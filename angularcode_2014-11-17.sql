# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.20)
# Database: angularcode
# Generation Time: 2014-11-17 22:32:15 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `credit_type` enum('CT','DT') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `credit_type`)
VALUES
	(7,'Rent','DT'),
	(12,'Income','CT'),
	(13,'Andrews Test Network','DT'),
	(14,'Andrews Test Network','DT'),
	(15,'Andrews Test Network','DT'),
	(16,'Andrews Test Network','DT');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table customers_auth
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customers_auth`;

CREATE TABLE `customers_auth` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `address` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `customers_auth` WRITE;
/*!40000 ALTER TABLE `customers_auth` DISABLE KEYS */;

INSERT INTO `customers_auth` (`uid`, `name`, `email`, `phone`, `password`, `address`, `city`, `created`)
VALUES
	(169,'Swadesh Behera','swadesh@gmail.com','1234567890','$2a$10$251b3c3d020155f7553c1ugKfEH04BD6nbCbo78AIDVOqS3GVYQ46','4092 Furth Circle','Singapore','2014-08-31 18:21:20'),
	(170,'Ipsita Sahoo','ipsita@gmail.com','1111111111','$2a$10$d84ffcf46967db4e1718buENHT7GVpcC7FfbSqCLUJDkKPg4RcgV2','2, rue du Commerce','NYC','2014-08-31 18:30:58'),
	(171,'Trisha Tamanna Priyadarsini','trisha@gmail.com','2222222222','$2a$10$c9b32f5baa3315554bffcuWfjiXNhO1Rn4hVxMXyJHJaesNHL9U/O','C/ Moralzarzal, 86','Burlingame','2014-08-31 18:32:03'),
	(172,'Sai Rimsha','rimsha@gmail.com','3333333333','$2a$10$477f7567571278c17ebdees5xCunwKISQaG8zkKhvfE5dYem5sTey','897 Long Airport Avenue','Madrid','2014-08-31 20:34:21'),
	(173,'Satwik Mohanty','satwik@gmail.com','4444444444','$2a$10$2b957be577db7727fed13O2QmHMd9LoEUjioYe.zkXP5lqBumI6Dy','Lyonerstr. 34','San Francisco\n','2014-08-31 20:36:02'),
	(174,'Tapaswini Sahoo','linky@gmail.com','5555555555','$2a$10$b2f3694f56fdb5b5c9ebeulMJTSx2Iv6ayQR0GUAcDsn0Jdn4c1we','ul. Filtrowa 68','Warszawa','2014-08-31 20:44:54'),
	(175,'Manas Ranjan Subudhi','manas@gmail.com','6666666666','$2a$10$03ab40438bbddb67d4f13Odrzs6Rwr92xKEYDbOO7IXO8YvBaOmlq','5677 Strong St.','Stavern\n','2014-08-31 20:45:08'),
	(178,'AngularCode Administrator','admin@angularcode.com','0000000000','$2a$10$72442f3d7ad44bcf1432cuAAZAURj9dtXhEMBQXMn9C8SpnZjmK1S','C/1052, Bangalore','','2014-08-31 21:00:26'),
	(187,'Naga Penmetsa','deepu4w@gmail.com','+447885830218','$2a$10$f9d0a2a6022820fb7cd44usge7bQPv0S1Dw4LNSxtloqvPm3iVU4K','12 Beckford Gardens','','2014-11-13 22:16:22');

/*!40000 ALTER TABLE `customers_auth` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
