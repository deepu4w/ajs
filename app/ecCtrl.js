app.controller('ecCtrl', function ($scope,$http,CategoryService, Data, ngDialog) {
    //initially set those objects to null to avoid undefined error
	$scope.date = new Date();
	
	$http.get('api/ec/items.php').
    success(function(data) {
    	$scope.items = data;
    });
	
    $scope.addItem = function (i) {
    	$http.post('api/ec/items.php',i).success(function(results){
    		Data.toast(results);
    		$scope.items.push({
        				category: i.category,
            			title: i.title,
        				desc: i.desc,
        				amount: i.amount
        			});
    		})
    };
    
    $scope.delItem = function (i) {
    	console.log(i);
    	$http.delete('api/ec/items.php/'+i.id).success(function(results){
    		$scope.items.splice($scope.items.indexOf(i), 1);
    		Data.toast(results);
            
    	});
    };
    
    
    $scope.editItem = function (p,size) {
    	ngDialog.open({ template: 'partials/ec/edit-item.html' });
    };
    	
    
    
   // Categories 
   $http.get('api/ec/categories.php').
            success(function(data) {
                $scope.categories = data;
            });
   
   $scope.addCategory = function (i) {
	   $http.post('api/ec/categories.php',i).
	   	success(function() {
	   		$scope.categories.push(
	   	   			{
	   	   				name: i.name,
	   	       			credit_type: i.credit_type,
	   	   			
	   	   			}
	   	   		);
	   });
	   
   	
   };
   
   $scope.removeCategory = function (i) {
	   $http.delete('api/ec/categories.php/'+i.id).
	   	success(function() {
	   		$scope.categories.splice($scope.categories.indexOf(i), 1);
	   });
	   
   	
   }
   
      
    
});