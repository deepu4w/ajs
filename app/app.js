var app = angular.module('myApp', ['ngRoute', 'ngAnimate', 'toaster', 'ngDialog']);

app.config(['$routeProvider',
  function ($routeProvider) {
        $routeProvider
	        .when('/login', {
	            title: 'Login',
	            templateUrl: 'partials/login.html',
	            controller: 'authCtrl'
	        })
            .when('/logout', {
                title: 'Logout',
                templateUrl: 'partials/login.html',
                controller: 'logoutCtrl'
            })
            .when('/signup', {
                title: 'Signup',
                templateUrl: 'partials/signup.html',
                controller: 'authCtrl'
            })
            .when('/dashboard', {
                title: 'Dashboard',
                templateUrl: 'partials/dashboard.html',
                controller: 'authCtrl'
            })
            .when('/', {
                title: 'Login',
                templateUrl: 'partials/login.html',
                controller: 'authCtrl',
                role: '0'
            })
            .when('/ec', {
                title: 'Dashboard',
                templateUrl: 'partials/ec/index.html',
                controller: 'ecCtrl'
            }) 
            .when('/ec/add-category', {
                title: 'Dashboard',
                templateUrl: 'partials/ec/category.html',
                controller: 'ecCtrl'
            })
           
            .otherwise({
                redirectTo: '/login'
            });
  }])
    .run(function ($rootScope, $location, Data) {
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            $rootScope.authenticated = false;
            Data.get('session').then(function (results) {
                if (results.uid) {
                    $rootScope.authenticated = true;
                    $rootScope.uid = results.uid;
                    $rootScope.name = results.name;
                    $rootScope.email = results.email;
                    
                    // redirect from login page if already logged in
                    var nextUrl = next.$$route.originalPath;
                    if (nextUrl == '/login' || nextUrl == '/') {
                    	$location.path("/dashboard");
                    }
                } else {
                    var nextUrl = next.$$route.originalPath;
                    if (nextUrl == '/signup' || nextUrl == '/login') {
                   
                    } else {
                        $location.path("/login");
                    }
                }
            });
        });
    });