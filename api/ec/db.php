<?php
require_once __DIR__.'/../libs/ActiveRecord.php';
// assumes a table named "books" with a pk named "id"
// see simple.sql
class Db extends ActiveRecord\Model { }
// initialize ActiveRecord
// change the connection settings to whatever is appropriate for your mysql server 
ActiveRecord\Config::initialize(function($cfg)
{
    $cfg->set_model_directory(__DIR__.'/../models/');
    $cfg->set_connections(array('development' => 'mysql://root:root@127.0.0.1/angularcode'));
});


?>