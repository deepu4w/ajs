<?php
require_once 'common.php';


// setting response content type to json
$app->contentType('application/json');


function delC()
{
	$parameters = array();

	// first of all, pull the GET vars
	if (isset($_SERVER['REQUEST_URI'])) {
		$temp_arr = explode('/', $_SERVER['REQUEST_URI']);
	}
	$id = $temp_arr[count($temp_arr)-1];
	if(is_numeric($id)){
		$post = Item::find($id);
		$post->delete();
		$res = array(
			'status' => true,
			'message' => 'Item ' .$post->title. ' ' .$id. ' deleted.'
		);
		echo json_encode($res);
	} else {
		echo $id;
	}
}

function getC()
{

	$res = Item::find('all');
	$array = array();
	$i = 0;
	foreach ($res as $r) {
		$array[$i] = $r->attributes();
		$category = $array[$i]['category'];
		// Get categories using id 
		$temp = Category::find($category);
		$array[$i]['category'] = $temp->attributes();
		$i++;

	}
	
	echo json_encode($array);
	
}


function postC() {
	$body = file_get_contents("php://input");
	$content_type = false;
	if(isset($_SERVER['CONTENT_TYPE'])) {
		$content_type = $_SERVER['CONTENT_TYPE'];
	}
	if(strpos($content_type,"application/json") !== false) {
		// json to object
		$arr = json_decode($body);
		//     print_r($arr);
		$category = $arr->category;
		// saving the data
		$post = new Item();
		$post->category = $category->id;
		$post->title = $arr->title;
		$post->desc = $arr->desc;
		$post->amount = $arr->amount;
		$post->save();
		
		$res = array(
			'status' => true,
			'message' => 'Item ' .$post->title. ' creted.'
		);
		echo json_encode($res);
	} else {
		$res = array(
			'status' => false,
			'message' => 'Soemthing went wrong'
		);
		echo json_encode($res);
	}

}

