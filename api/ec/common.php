<?php
require '.././libs/Slim/Slim.php';
require_once 'db.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$method = $_SERVER['REQUEST_METHOD'];

switch ($method) {
    case 'GET':
        getC();
        break;

    case 'POST':
        postC();
        break;

    case 'DELETE':
        delC();
        break;

    case 'PUT':
        updateC();
        break;

    default:
        header('HTTP/1.1 405 Method Not Allowed');
        header('Allow: GET, POST, PUT, DELETE');
        break;
}
