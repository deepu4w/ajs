<?php
require_once 'common.php';


// setting response content type to json
$app->contentType('application/json');


function delC()
{
    $parameters = array();

    // first of all, pull the GET vars
    if (isset($_SERVER['REQUEST_URI'])) {
        $temp_arr = explode('/', $_SERVER['REQUEST_URI']);
    }
    $id = $temp_arr[count($temp_arr)-1];
    if(is_numeric($id)){
        $post = Category::find($id);
        $post->delete();
        echo 'category ' .$post->name. ' deleted.';
    }
}

function getC()
{

    $res = Category::find('all');
    $array = array();
    foreach ($res as $r) {
        $array[] = $r->attributes();
    }

    echo json_encode($array);
}


function postC() {
    $body = file_get_contents("php://input");
    $content_type = false;
    if(isset($_SERVER['CONTENT_TYPE'])) {
        $content_type = $_SERVER['CONTENT_TYPE'];
    }
    if(strpos($content_type,"application/json") !== false) {
        // json to object
        $arr = json_decode($body);

        // saving the data
        $post = new Category();
        $post->name = $arr->name;
        $post->credit_type = $arr->credit_type;
        $post->save();
    } else {
        die($content_type);
    }

}

